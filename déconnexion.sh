#!/bin/bash



#cryptage de la base de données 
FILE=disque/bdd.txt 
FILE2=RamDisk/pass1.clear
FILE3=RamDisk/pass2.clear

if [ -f "$FILE" ]; 
	then
	if [ -f "$FILE2" ]; 
		then
			if [ -f "$FILE3" ]; 
				then
						openssl enc -pbkdf2 -aes256 -in disque/bdd.txt -out disque/bddPart1Crypt.enc -kfile RamDisk/pass1.clear
						openssl enc -pbkdf2 -aes256 -in disque/bddPart1Crypt.enc -out disque/bdd.enc -kfile RamDisk/pass2.clear

						#suppression des clés et fichier en claire
						rm RamDisk/pass1.clear
						rm RamDisk/pass2.clear
						rm disque/bdd.txt
						rm disque/bddPart1Crypt.enc
						echo "Déconnexion"
				else
					echo "Erreur : la clé pass2.clear est manquante "

			fi
		else
			echo "Erreur : la clé pass1.clear est manquante "

	fi
	else
		echo "Erreur : la base de données bdd.txt est manquante (service déconnecté) "
fi

