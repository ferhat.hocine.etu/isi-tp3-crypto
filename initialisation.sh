#!/bin/bash

#création des dossiers

rm -rf disque RamDisk usb1 usb2
mkdir disque RamDisk usb1 usb2
touch disque/bdd.txt
echo "(nom,numéro)" >> disque/bdd.txt


#génération de clés

dd if=/dev/random bs=64 count=1 > RamDisk/pass1.clear
dd if=/dev/random bs=64 count=1 > RamDisk/pass2.clear

#cryptage des clés
#des mots de passe seront demandés pour chaque cryptage
echo "----------mot de passe responsable 1----------"
openssl enc -pbkdf2 -aes256 -in RamDisk/pass1.clear -out usb1/pass1.enc
echo "----------mot de passe responsable 2----------"
openssl enc -pbkdf2 -aes256 -in RamDisk/pass2.clear -out usb2/pass2.enc

#cryptage de la base de données 
openssl enc -pbkdf2 -aes256 -in disque/bdd.txt -out disque/bddPart1Crypt.enc -kfile RamDisk/pass1.clear
openssl enc -pbkdf2 -aes256 -in disque/bddPart1Crypt.enc -out disque/bdd.enc -kfile RamDisk/pass2.clear


#suppression des clés et fichier en claire

rm RamDisk/pass1.clear
rm RamDisk/pass2.clear
rm disque/bdd.txt
rm disque/bddPart1Crypt.enc

echo "----------Initialisation terminée----------"

