#!/bin/bash


FILE=../disque/bdd.txt 

echo "********** Retrait des droit  **********"
read -p "Choissir la personne répudier (RES1,RES2,REP1,REP2) : " PERSONNE


if [ $PERSONNE = "RES1" ]
then
	echo "veuillez saisir les mots de passe -a) Représentant1 -b) Représentant2  -c) Résponsable2 "
	if openssl enc -d -pbkdf2 -aes256 -in ../usb1Rep/pass1.enc -out ../RamDisk/pass1.clear && openssl enc -d -pbkdf2 -aes256 -in ../usb2Rep/pass2.enc -out ../RamDisk/pass2.clear  && openssl enc -d -pbkdf2 -aes256 -in ../usb2/pass2.enc -out ../RamDisk/pass2.clear  
		then 
		# suppression de la ligne 'ThisIsRepresentent'
		sed "/ThisIsRepresentent/d" ../RamDisk/pass1.clear > ../RamDisk/pass1Inter.clear
		rm ../RamDisk/pass1.clear
		mv ../RamDisk/pass1Inter.clear ../RamDisk/pass1.clear
		#décryptage de la base de données
		openssl enc -d -pbkdf2 -aes256 -in ../disque/bdd.enc -out ../disque/bddPart1Crypt.enc -kfile ../RamDisk/pass2.clear
		openssl enc -d -pbkdf2 -aes256 -in ../disque/bddPart1Crypt.enc -out ../disque/bdd.txt -kfile ../RamDisk/pass1.clear
		rm ../disque/bddPart1Crypt.enc
		rm ../disque/bdd.enc
		# géneration de la nouvelle clé responsable1
		dd if=/dev/random bs=64 count=1 > ../RamDisk/pass1.clear
		cp ../RamDisk/pass1.clear  ../RamDisk/pass1Représentant.clear
		sed -i '1iThisIsRepresentent' ../RamDisk/pass1Représentant.clear
		echo "veuillez saisir le mots de passe du nouveau résponsable1"
		openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass1.clear -out ../usb1/pass1.enc
		echo "veuillez saisir a nouveau le mots de passe du représentant1"
		openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass1Représentant.clear -out ../usb1Rep/pass1.enc
		#Cryptage de la base de données avec les nouvelle clés
		openssl enc -pbkdf2 -aes256 -in ../disque/bdd.txt -out ../disque/bddPart1Crypt.enc -kfile ../RamDisk/pass1.clear
		openssl enc -pbkdf2 -aes256 -in ../disque/bddPart1Crypt.enc -out ../disque/bdd.enc -kfile ../RamDisk/pass2.clear
		rm ../RamDisk/pass1.clear
		rm ../RamDisk/pass1Représentant.clear
		rm ../RamDisk/pass2.clear
		rm ../disque/bdd.txt
		rm ../disque/bddPart1Crypt.enc


	else 
		echo "Erreur mot de passe "
		
	fi
	
elif [ $PERSONNE = "RES2" ]
then
	echo "veuillez saisir les mots de passe -a) Représentant1 -b) Représentant2  -c) Résponsable1 "
	if openssl enc -d -pbkdf2 -aes256 -in ../usb1Rep/pass1.enc -out ../RamDisk/pass1.clear && openssl enc -d -pbkdf2 -aes256 -in ../usb2Rep/pass2.enc -out ../RamDisk/pass2.clear  && openssl enc -d -pbkdf2 -aes256 -in ../usb1/pass1.enc -out ../RamDisk/pass1.clear  
		then 
		# suppression de la ligne 'ThisIsRepresentent'
		sed "/ThisIsRepresentent/d" ../RamDisk/pass2.clear > ../RamDisk/pass2Inter.clear
		rm ../RamDisk/pass2.clear
		mv ../RamDisk/pass2Inter.clear ../RamDisk/pass2.clear
		#décryptage de la base de données
		openssl enc -d -pbkdf2 -aes256 -in ../disque/bdd.enc -out ../disque/bddPart1Crypt.enc -kfile ../RamDisk/pass2.clear
		openssl enc -d -pbkdf2 -aes256 -in ../disque/bddPart1Crypt.enc -out ../disque/bdd.txt -kfile ../RamDisk/pass1.clear
		rm ../disque/bddPart1Crypt.enc
		rm ../disque/bdd.enc
		# géneration de la nouvelle clé responsable1
		dd if=/dev/random bs=64 count=1 > ../RamDisk/pass2.clear
		cp ../RamDisk/pass2.clear  ../RamDisk/pass2Représentant.clear
		sed -i '1iThisIsRepresentent' ../RamDisk/pass2Représentant.clear
		echo "veuillez saisir le mots de passe du nouveau résponsable2"
		openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass2.clear -out ../usb2/pass2.enc
		echo "veuillez saisir a nouveau le mots de passe du représentant2"
		openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass2Représentant.clear -out ../usb2Rep/pass2.enc
		#Cryptage de la base de données avec les nouvelle clés
		openssl enc -pbkdf2 -aes256 -in ../disque/bdd.txt -out ../disque/bddPart1Crypt.enc -kfile ../RamDisk/pass1.clear
		openssl enc -pbkdf2 -aes256 -in ../disque/bddPart1Crypt.enc -out ../disque/bdd.enc -kfile ../RamDisk/pass2.clear
		rm ../RamDisk/pass1.clear
		rm ../RamDisk/pass2Représentant.clear
		rm ../RamDisk/pass2.clear
		rm ../disque/bdd.txt
		rm ../disque/bddPart1Crypt.enc
	else 
		echo "Erreur mot de passe "	
	fi

elif [ $PERSONNE = "REP1" ]
then
		echo "veuillez saisir les mots de passe -a) Représentant2 -b) Résponsable1  -c) Résponsable2 "
		if  openssl enc -d -pbkdf2 -aes256 -in ../usb2Rep/pass2.enc -out ../RamDisk/pass2.clear  && openssl enc -d -pbkdf2 -aes256 -in ../usb1/pass1.enc -out ../RamDisk/pass1.clear   && openssl enc -d -pbkdf2 -aes256 -in ../usb2/pass2.enc -out ../RamDisk/pass2.clear  
		then 
			#décryptage de la base de données
			openssl enc -d -pbkdf2 -aes256 -in ../disque/bdd.enc -out ../disque/bddPart1Crypt.enc -kfile ../RamDisk/pass2.clear
			openssl enc -d -pbkdf2 -aes256 -in ../disque/bddPart1Crypt.enc -out ../disque/bdd.txt -kfile ../RamDisk/pass1.clear
			rm ../disque/bddPart1Crypt.enc
			rm ../disque/bdd.enc
			# géneration de la nouvelle clé responsable1
			dd if=/dev/random bs=64 count=1 > ../RamDisk/pass1.clear
			cp ../RamDisk/pass1.clear  ../RamDisk/pass1Représentant.clear
			sed -i '1iThisIsRepresentent' ../RamDisk/pass1Représentant.clear
			echo "veuillez saisir le mots de passe du nouveau résponsable1"
			openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass1.clear -out ../usb1/pass1.enc
			echo "veuillez saisir a nouveau le mots de passe du représentant1"
			openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass1Représentant.clear -out ../usb1Rep/pass1.enc
			#Cryptage de la base de données avec les nouvelle clés
			openssl enc -pbkdf2 -aes256 -in ../disque/bdd.txt -out ../disque/bddPart1Crypt.enc -kfile ../RamDisk/pass1.clear
			openssl enc -pbkdf2 -aes256 -in ../disque/bddPart1Crypt.enc -out ../disque/bdd.enc -kfile ../RamDisk/pass2.clear
			rm ../RamDisk/pass1.clear
			rm ../RamDisk/pass1Représentant.clear
			rm ../RamDisk/pass2.clear
			rm ../disque/bdd.txt
			rm ../disque/bddPart1Crypt.enc
		else 
			echo "Erreur mot de passe "	
		fi

elif [ $PERSONNE = "REP2" ]
	then
		echo "veuillez saisir les mots de passe -a) Représentant1 -b) Résponsable1  -c) Résponsable2 "
		if  openssl enc -d -pbkdf2 -aes256 -in ../usb1Rep/pass1.enc -out ../RamDisk/pass1.clear  && openssl enc -d -pbkdf2 -aes256 -in ../usb1/pass1.enc -out ../RamDisk/pass1.clear   && openssl enc -d -pbkdf2 -aes256 -in ../usb2/pass2.enc -out ../RamDisk/pass2.clear  
		then 
			#décryptage de la base de données
			openssl enc -d -pbkdf2 -aes256 -in ../disque/bdd.enc -out ../disque/bddPart1Crypt.enc -kfile ../RamDisk/pass2.clear
			openssl enc -d -pbkdf2 -aes256 -in ../disque/bddPart1Crypt.enc -out ../disque/bdd.txt -kfile ../RamDisk/pass1.clear
			rm ../disque/bddPart1Crypt.enc
			rm ../disque/bdd.enc
			# géneration de la nouvelle clé responsable1
			dd if=/dev/random bs=64 count=1 > ../RamDisk/pass2.clear
			cp ../RamDisk/pass2.clear  ../RamDisk/pass2Représentant.clear
			sed -i '1iThisIsRepresentent' ../RamDisk/pass2Représentant.clear
			echo "veuillez saisir le mots de passe du nouveau résponsable2"
			openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass2.clear -out ../usb2/pass2.enc
			echo "veuillez saisir a nouveau le mots de passe du représentant2"
			openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass2Représentant.clear -out ../usb2Rep/pass2.enc
			#Cryptage de la base de données avec les nouvelle clés
			openssl enc -pbkdf2 -aes256 -in ../disque/bdd.txt -out ../disque/bddPart1Crypt.enc -kfile ../RamDisk/pass1.clear
			openssl enc -pbkdf2 -aes256 -in ../disque/bddPart1Crypt.enc -out ../disque/bdd.enc -kfile ../RamDisk/pass2.clear
			rm ../RamDisk/pass1.clear
			rm ../RamDisk/pass2Représentant.clear
			rm ../RamDisk/pass2.clear
			rm ../disque/bdd.txt
			rm ../disque/bddPart1Crypt.enc
		else 
			echo "Erreur mot de passe "	
		fi
else
	echo "ERREUR : choisir une réponse parmis les proposition"

fi



