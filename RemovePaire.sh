#!/bin/bash


#supprimer une paire 
if [ $# -eq 2 ]
  then
	FILE=disque/bdd.txt
	if [ -f "$FILE" ]; 
		then
			line="("$1","$2")"
			if grep "$line" "$FILE"
			  then
				sed "/$line/d" disque/bdd.txt > disque/bdd2.txt 
				rm disque/bdd.txt 
				mv disque/bdd2.txt disque/bdd.txt 
				echo "Paire ("$1","$2") supprimée"
			   else
			   	echo "Erreur : la paire ("$1","$2") n'existe pas"
			  fi

		else
			echo "Erreur : Le service n'est pas en cours d'éxécution veuillez d'abords lancer le service"
	fi
   else
		echo "Erreur : Nombre d'arguments différent de deux "
fi



