# Rendu "Crypto"

## Binome

Mokeddes,Youva, email:youva.mokeddes.etu@univ-lille.fr
Hocine, Ferhat, email:ferhat.hocine.etu@univ-lille.fr


## Question 1
```
la solution de l'authentification à deux facteur :
est d'avoir deux clefs usb différente qui contiennet une clefs de chiffrement chacune protégé par un mot de passe.
l'authentification ce fait par un chiffrement de la clef 1 après acces avec le mot de passe correspond puis un 2 ème chiffrement pour le résultat avec la 2ème clef accessible par mot de passe aussi.
```


## Question 2
```
le script initialisation.sh permet de créer les 2 clés usb ainsi que la clé de cryptage avec mot de passe dans chaque clé usb
le script service1.sh(/Version1)  permet de la mise en service en décryptant les 2 clés de cryptage à partir des clés usb puis décryptant la bdd (/disque) une première fois avec la clé2 puis un autre cryptage pour le résultat avec la clé1
le script addPaire permet l'ajout d'une paire si le services est en cours de marche et que 2 arguments sont passés en paramètre
le script RemovePaire permet la supprission d'une paire si le services est en cours de marche et que 2 arguments sont passés en paramètre et finalement si la paire existe.
le script déconnexion met fin au service lancer en cryptant la base de données.

```

## Question 3
```
la solution proposer est de créer deux autres clé usb (usb1Rep, usb2Rep) pour les représentants et de crées deux fichier coréspondant au clé de cryptage de chauqe clé usb.
puis une ligne 'ThisIsRepresentent' sera ajouter au début de chaque clé de cryptage puis seront crypter de la même manier que les clé des représentant avec des mots de passes différent.
les fichier claire seront supprimé.
Dans la mise en service le nom des clé sera demander à l'utilisateur ( ex : usb1, usb1Rep) et la clé de cryptage sera utilisé à partire de la clé choisi et après authentification.
Notre script vérifier si la clé choisi commence par la ligne 'ThisIsRepresentent' si oui il supprime la ligne et renvoie que l'utilisateur est un représentant et continue de la même méthode pour décrypté la 2 ème clé.
Au final il décrypte la base de données en utilisant la 2ème clé puis la 1ere et supprime ces clé en claire.

```

## Question 4
```
Dans (version2_représentant)
initialisation2.sh crée  les dossiers pour les clés usb des représentant puis crée les clés de cryptage.
service2.sh permet la mise en service de notre seconde version (comme expliqué dans la question3)

(addPaire,RemovePaire,deconnexion) dans la racine du projet fonctionnent de la mếme maniere pour les 2 version de service.
```


## Question 5
```
la solution proposer est un script qui demande à l'utilisateur de choisir la personne a répudier entre résponsable1,résponsable2,Représentant1 et Représentant2.
une fois la personne choisi l'authentification des trois autres est requise afin de créer une clé de cryptage pour la nouvelle personne qui occupera le poste ainsi que la clé de la personne relier à elle avec la relation (Résponsable,Représentant2).
puis la base de donnée (décrypté au début du script) est recrypté avec les nouvelles clés généré.
```
	
## Question 6
```
Dans (version3_répudiation)
service3.sh permet de répudier un résponsable ou un représentant 
on peut à la fin utilisé les autre service afin de le mettre en marche ainsi que les scripts(addPaire,RemovePaire,deconnexion) dans la racine du projet pour des fonctions suplémentaire.
```
