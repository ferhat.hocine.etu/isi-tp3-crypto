#!/bin/bash



#cryptage des clés
#les mots de passe sont requis pour le décryptage

FILE=../disque/bdd.txt 


if [ -f "$FILE" ]; 
	then
		echo "le service est en marche "
	else
		read -p "entrer le nom de la clé usb 1 (ex: usb1 , usb1Rep): " usb
		echo "----------mot de passe du résponsable de la clé 1----------"
		FILE2="../"$usb"/pass1.enc"
		if [ -f "$FILE2" ]; 
			then
				openssl enc -d -pbkdf2 -aes256 -in "../"$usb"/pass1.enc" -out ../RamDisk/pass1.clear
				if grep "ThisIsRepresentent" ../RamDisk/pass1.clear
					  then
						sed "/ThisIsRepresentent/d" ../RamDisk/pass1.clear > ../RamDisk/pass1Inter.clear
						rm ../RamDisk/pass1.clear
						mv ../RamDisk/pass1Inter.clear ../RamDisk/pass1.clear
						echo "vous êtes un représentant"
				fi
			else
				echo "ERREUR : fichier pass1.enc introuvable dans "$usb" "	
		fi
		
		read -p "entrer le nom de la clé usb 2 (ex: usb2 , usb2Rep): " usb2
		echo "----------mot de passe du résponsable de la clé 2----------"
		FILE3="../"$usb2"/pass2.enc"
		if [ -f "$FILE3" ]; 
			then
				openssl enc -d -pbkdf2 -aes256 -in "../"$usb2"/pass2.enc" -out ../RamDisk/pass2.clear
				if grep "ThisIsRepresentent" ../RamDisk/pass2.clear
					  then
						sed "/ThisIsRepresentent/d" ../RamDisk/pass2.clear > ../RamDisk/pass2Inter.clear
						rm ../RamDisk/pass2.clear
						mv ../RamDisk/pass2Inter.clear ../RamDisk/pass2.clear
						echo "vous êtes un représentant"
						
				fi
				openssl enc -d -pbkdf2 -aes256 -in ../disque/bdd.enc -out ../disque/bddPart1Crypt.enc -kfile ../RamDisk/pass2.clear
				openssl enc -d -pbkdf2 -aes256 -in ../disque/bddPart1Crypt.enc -out ../disque/bdd.txt -kfile ../RamDisk/pass1.clear

				rm ../disque/bddPart1Crypt.enc
				rm ../disque/bdd.enc
				echo "----------service en marche----------"
			else
				echo "ERREUR : fichier pass1.enc introuvable dans "$usb" "	
		fi
fi



