#!/bin/bash

#création des dossiers

rm -rf ../usb1Rep ../usb2Rep
mkdir ../usb1Rep ../usb2Rep



echo "----------mot de passe responsable 1----------"
openssl enc -d -pbkdf2 -aes256 -in ../usb1/pass1.enc -out ../RamDisk/pass1.clear
echo "----------mot de passe responsable 2----------"
openssl enc -d -pbkdf2 -aes256 -in ../usb2/pass2.enc -out ../RamDisk/pass2.clear
echo "----------service en marche----------"
		
mv ../RamDisk/pass1.clear  ../RamDisk/pass1Représentant.clear
mv ../RamDisk/pass2.clear  ../RamDisk/pass2Représentant.clear

sed -i '1iThisIsRepresentent' ../RamDisk/pass1Représentant.clear
sed -i '1iThisIsRepresentent' ../RamDisk/pass2Représentant.clear


echo "**********   mot de passe pour les représentant    **********   "
echo "----------mot de passe représentant 1----------"
openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass1Représentant.clear -out ../usb1Rep/pass1.enc
echo "----------mot de passe représentant 2----------"
openssl enc -pbkdf2 -aes256 -in ../RamDisk/pass2Représentant.clear -out ../usb2Rep/pass2.enc
		


#suppression des clés et fichier en claire


rm ../RamDisk/pass1Représentant.clear
rm ../RamDisk/pass2Représentant.clear
